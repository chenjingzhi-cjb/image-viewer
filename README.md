# ImageViewer (C++ QT)

该图像浏览器基于 C++ QT 开发，主要目的是帮助机器视觉相关开发者能够更方便地浏览图像。



## 界面

[![p9zPYhq.png](https://s1.ax1x.com/2023/06/01/p9zPYhq.png)](https://imgse.com/i/p9zPYhq)



## 环境

编译环境：MinGW 64-bit

Release 构建环境：QT5